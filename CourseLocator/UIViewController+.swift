//
//  UIViewController+.swift
//  CourseLocator
//
//  Created by Raymond on 8/9/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // MARK: Public Functions
    // present an UIAlertViewController with the give heading and message
    func showAlert(heading:UIAlertController.Heading, message:String) {
        
        let alertController = UIAlertController.QuickMessage(heading, message: message)
        presentViewController(alertController, animated: true, completion: nil)
    }
}