//
//  TextFormat.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import UIKit

class TextFormat:NSObject  {
	
	let plistName = "TextFormats"
	
    // MARK: Properties
    class Keys {

        static let textColor = "textColor"
        static let fontName = "fontName"
        static let fontSize = "fontSize"
		
		static func allKeys() -> [String] {
			return [Keys.fontName, Keys.textColor, Keys.fontSize]
		}
    }
    
    class Formats {
        
        static let courseTitleFormat = "CourseTitleFormat"
        static let courseDetailFormat = "CourseDetailFormat"
    }
    
    var fontName:String?
    var fontSize:CGFloat?
    var textColor:String?
    
    // MARK: Init
	convenience init(keyPaths:[String], bundle:NSBundle? = nil) {
        
        self.init()
        let b = bundle?.bundleIdentifier ?? NSBundle.mainBundle().bundleIdentifier

        if let dict = Utilities.bundlePlistRootAsDictionary(plistName, bundleIdentifier:b!) {
            
            if let formatData = dataForPath(dict, keyPaths:keyPaths) {
                
                // relect the TextFormat object as Keys to pull from plist
                for key in Keys.allKeys() {
                    
                    // if value exist in plist set value
                    if let value = formatData[key] as? String {
                        setValue(value, forKey: key)
                    }
					else if key == "fontSize" {
						if let value =  formatData[key] as? CGFloat {
							fontSize = value
						}
					}
                }
            }
        }
    }

    // MARK: Private Functions
    // Get data from TextFormats plist
    private func dataForPath(dict:Dictionary<String, AnyObject>, keyPaths:[String]) -> Dictionary<String, AnyObject>? {
        
        var dictionaryData = dict
        for key in keyPaths {
            
            //recursively moves thru dictionary with keys
            if let dictionaryDataForPath = dictionaryData[key] as? Dictionary<String, AnyObject> {
                dictionaryData = dictionaryDataForPath
            }
        }

        return dictionaryData
    }
}

