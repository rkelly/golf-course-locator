//
//  Label.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class Label:UILabel {
    
    // MARK: Properties
    let rootKey = "Labels"
    
    @IBInspectable var formatKey:String = String(self.dynamicType).componentsSeparatedByString(".").last! {
        didSet{
            format = TextFormat(keyPaths: [rootKey, formatKey])
        }
    }
    
    var format:TextFormat = TextFormat() {
        didSet{
            setTextFormat()
        }
    }
    
    // MARK: Init
    init() {
        super.init(frame: CGRectZero)
        self.format = TextFormat(keyPaths: [rootKey, formatKey])
        setTextFormat()
        setStyle()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.format = TextFormat(keyPaths: [rootKey, formatKey])
        setTextFormat()
        setStyle()
    }
    
    init(format:TextFormat) {
        super.init(frame: CGRectZero)
        self.format = format
        setTextFormat()
        setStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.format = TextFormat(keyPaths: [rootKey, formatKey])
        setTextFormat()
        setStyle()
    }
    
    // MARK: Public Functions
    // set text format
    func setTextFormat() {
		
		
        if let fontName = format.fontName, fontSize = format.fontSize,
			value = UIFont(name: fontName, size: fontSize) {
			  font = value
        }
        
        if let value = format.textColor, color = UIColor.color(value) {
            textColor = color
        }
    }
    
    
    
    // Override any additional Styling here
    func setStyle() {
        
    }
}
