//
//  LocationOrigin.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/8/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import MapKit


class LocationOrigin:NSObject, MKAnnotation {
    
    // MARK: Properties
    struct Keys {
        static let location:CLLocation = CLLocation(latitude: 40.814381, longitude: -96.710226)
    }
    
    var title:String? {
        return "origin".localization()
    }
    
    var coordinate:CLLocationCoordinate2D {
        return LocationOrigin.Keys.location.coordinate
    }
}