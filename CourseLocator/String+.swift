//
//  String+.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation


extension String {
    
    // MARK: Public Functions
    // grabs strings form string tables, default file is English.strings
    func localization(tableName:String = "English") -> String {
        return NSLocalizedString(self, tableName: tableName, comment: "")
    }
}