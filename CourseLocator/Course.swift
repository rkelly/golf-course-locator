//
//  Course.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import Parse
import MapKit

class Course:BaseParseObject, PFSubclassing, MKAnnotation {
    
    // MARK: Properties
    struct Keys {
        static let parseClassName = "Course"
    }
    
    struct DataMapping {
        static let id = "id"
        static let name = "name"
        static let location = "location"
    }
    
    var name:String = ""
    var holes:Int = 0
    var street:String = ""
    var zip:Int = 0
    var par:Int = 0
    var yards:Int = 0
    var location:PFGeoPoint?
    var selected:Bool = false
    
    var coordinate: CLLocationCoordinate2D {
        get{
            return locationCoordinate2D()
        }
    }
    
    // MARK: MKAnnotation Properties
    var title:String? {
        return name
    }
    
    var subtitle:String? {
        return "\(holes) Holes, Course Par:\(par) Total Yards:\(yards)"
    }
    
    // MARK: Class Functions
    // Parse Class Name
    class func parseClassName() -> String {
        return Course.Keys.parseClassName
    }
    
    // Sorts array of courses by the distance from a given location
    class func sortedCoursesByDistance(courses:[Course], ascending:Bool, fromLocation:CLLocation) -> [Course] {
        
        return courses.sort({ (Course1, Course2) -> Bool in
            
            return Course1.distanceFromCourseLocation(fromLocation) < Course2.distanceFromCourseLocation(fromLocation) ?
                ascending : !ascending
        })
    }
    
    
    // MARK: Overrides Functions
    // Binds Paramaters Types not accounted for by Super Class
    override func updateValues() {
        super.updateValues()
        
        if let location = self[Course.DataMapping.location] as? PFGeoPoint {
            self.location = location
        }
    }
    
    
    // MARK: Private Functions
    // Uses the PFGeoPoint object to return a CLLocationCooridate2D
    private func locationCoordinate2D() -> CLLocationCoordinate2D {
        
        let lat = location != nil ? location!.latitude : 0
        let long = location != nil ? location!.longitude : 0
        
        return CLLocationCoordinate2D(latitude: lat, longitude: long)
        
    }
    
    // MARK: Public Functions
    //  Calculates the distance from the Course to a given location in Miles
    func distanceFromCourseLocation(cllocation:CLLocation) -> Double? {
        
        if let courseLat = location?.latitude, courseLong = location?.longitude {
            
            let courseLocation = CLLocation(latitude: courseLat, longitude: courseLong)
            let distance = cllocation.distanceFromLocation(courseLocation)
                
            return Utilities.convertMetersToMiles(distance)
        }
        
        return nil
    }
    
    // Colors the Course Annotation when Selected and Deselected
    func setAnnotationColor(annotationView: MKAnnotationView) {
        
        if let pinAnnotationView = annotationView as? MKPinAnnotationView {
            pinAnnotationView.pinColor = selected ? .Purple : .Green
        }
    }
}