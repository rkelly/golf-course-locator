//
//  UIColor+.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    // MARK: Class Functions
    // Creates UIColor from string value
    class func color(hex:NSString, alpha:CGFloat = 1.0) -> UIColor? {
        
        if hex.length != 6 {
            return nil
        }
        
        let rString = hex.substringToIndex(2)
        let gString = (hex.substringFromIndex(2) as NSString).substringToIndex(2)
        let bString = (hex.substringFromIndex(4) as NSString).substringToIndex(2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        let red:CGFloat = CGFloat(r) /  255.0
        let green:CGFloat = CGFloat(g) / 255.0
        let blue:CGFloat = CGFloat(b) / 255.0
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}