//
//  ViewController.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/6/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import UIKit
import MapKit


class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MKMapViewDelegate {

    // MARK: Properties
    @IBOutlet weak var mapView:MKMapView!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var headingLabel:Label!
    
    var courses:[Course] = [] {
        didSet{
            emptyState.hidden = courses.count == 0 ? false : true
            collectionView.reloadData()
            mapView.addAnnotations(courses)
        }
    }

    let locationOrigin = LocationOrigin()
    var annontationViewTapGestureRecongizer:UITapGestureRecognizer!
    var emptyState:EmptyState!
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setNeedsStatusBarAppearanceUpdate()
        
        headingLabel.text = "appTitle".localization()
        
        //setup collectionView
        collectionView.dataSource = self
        collectionView.delegate = self
        
        // display location origin
        mapView.delegate = self
        mapView.updateMapView(LocationOrigin.Keys.location, radius: 0.1, animated: true)
        
        // fetch courses
        fetchCourses()
        
        // add locationOrigin Annotation
        mapView.addAnnotation(locationOrigin)
        
        // custom tap recongizer to deselect annotations
        annontationViewTapGestureRecongizer = UITapGestureRecognizer(target: self, action: "annotationViewTapped:")
        
        // create empty state
        emptyState = EmptyState(message: "emptyStateMessage".localization())
        collectionView.backgroundView = emptyState
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        view.layoutIfNeeded()
        emptyState.updateLayout(collectionView.bounds)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    // MARK: IBActions
    // Presents the UIActivityViewController to share Course data
    @IBAction func shareButtonPressed(sender: AnyObject) {
        
        let coursesSelected = selectedCourses()
        if coursesSelected.count == 0 {
            
            showAlert(.Warn, message: "noCoursesSelectedWarning".localization())
            return
        }
        
        let doubleSpace = "\n\n"
        var sharingItems:[AnyObject] = ["shareMessage".localization(), doubleSpace]
        for course in coursesSelected {
            
            var courseInfo:[AnyObject] = [course.name]
            
            if let courseLocation = course.location {
                
                let safeName = course.name.stringByReplacingOccurrencesOfString(" ", withString: "+")
                let appleMapsLink = "appleMapsLink".localization()
                courseInfo.append(NSURL(string:"\(appleMapsLink)?ll=\(courseLocation.latitude),\(courseLocation.longitude)&q=\(safeName)")!)
            }
            
            courseInfo.append(doubleSpace)
            courseInfo.append(doubleSpace)
            sharingItems += courseInfo
        }
        
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    // Clear data (if any) and refetch
    @IBAction func resetButtonPressed(sender: AnyObject) {
        
        courses = []
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(locationOrigin)
        fetchCourses()
    }
    
    
    // MARK: Public Functions
    // fetch Courses in Located within 50 miles of LocationOrigin
    func fetchCourses() {
        
        parseManager.coursesByLocation(LocationOrigin.Keys.location, withInMiles:50, completion: { (response, error) -> () in
            
            if let error = error {

                errorManager.handleNSErrors(error, completion: { (showUser, response) -> () in
                    if showUser {
                        self.showAlert(.Error, message: response)
                    }
                })
            }
            
            if let courses = response {
                self.courses = courses
            }
        })
    }

    // MARK: Private Functions
    // Filter course by selected courses
    private func selectedCourses() -> [Course] {
        
        return courses.filter { $0.selected }
    }
    
    // Updates the map view when courses are selected and deselected
    private func reloadCourseAnnotationOnMap(course:Course) {
    
        // get the index of the course
        if let index = courses.indexOf(course) {
            
            let indexPath = NSIndexPath(forRow: index, inSection: 0)
            //scroll to index and reload cell
            collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
            collectionView.reloadItemsAtIndexPaths([indexPath])
        }
    }
    
    // MARK: Internal Functions
    // Detects if selected annotation is tapped for a 2nd time and deselects if true
    internal func annotationViewTapped(recongizer:UITapGestureRecognizer) {
        
        if let view = recongizer.view as? MKAnnotationView, course = view.annotation as? Course {
            
            view.removeGestureRecognizer(annontationViewTapGestureRecongizer)
            
            // by deselecting the annotation this will allow the didSelectAnnotationView function to fire
            mapView.deselectAnnotation(course, animated: true)
        }
    }

    // MARK: UICollectionView DataSource 
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CourseCell.identifier, forIndexPath: indexPath) as! CourseCell
        let course = courses[indexPath.row]
        cell.setupCellFromCourse(course)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return courses.count
    }
    
    // MARK: UICollectonView Delegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let course = courses[indexPath.row]
        course.selected = !course.selected
        collectionView.reloadItemsAtIndexPaths([indexPath])
        
        if let annotationView = mapView.viewForAnnotation(course) {
			course.setAnnotationColor(annotationView)
		}
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: CourseCell.cellHeight)
    }

    // MARK: MKMapViewDelegate
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        
        // if user selects a Course on the map, Select it in the collectionView
        if let course = view.annotation as? Course{
            
            if course.selected {
                mapView.deselectAnnotation(course, animated: true)
            }
            else {
                
                // add tap recongizer to deselect on 2nd Tap
                view.addGestureRecognizer(annontationViewTapGestureRecongizer)
            }
            
            // If the user selects the Course toggle the selection
            course.selected = !course.selected
            course.setAnnotationColor(view)
            reloadCourseAnnotationOnMap(course)
        }
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "pinAnnotation"
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        
        annotationView.pinColor = annotation.isEqual(locationOrigin) ? .Red : .Green
        annotationView.canShowCallout = true
        
        return annotationView
    }
}


