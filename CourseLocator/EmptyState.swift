//
//  EmptyState.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/10/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import UIKit

class EmptyState:UIView {
    
    
    // MARK: Properties
    let label:Label = Label()
    
    // MARK: Init
    convenience init(message:String) {
        self.init(frame:CGRectZero)
        createAddLabel(message)
    }
    
    // MARK: Public Functions
    // Set up the label used for an empty state
    func createAddLabel(message:String) {
    
        label.formatKey = TextFormat.Formats.courseDetailFormat
        label.textAlignment = .Center
        label.numberOfLines = 0 
        label.text = message
        
        addSubview(label)
    }
    
    // updates the view frame and the label
    func updateLayout(frame:CGRect) {
        
        self.frame = frame
        label.frame = bounds
    }
}