//
//  Constants.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import CoreLocation

let hudlHQLocation:CLLocation = CLLocation(latitude: 40.814381, longitude: -96.710226)
let shareMessage = "Check out theses Golf Courses when visiting the Hudl HQ."
let huddleLink = NSURL(string: "http://www.hudl.com")!
let doubleSpace = "\n\n"
let appleMaps = "http://maps.apple.com/"