//
//  BaseParseObject.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation

import Parse

class BaseParseObject:PFObject {

    // MARK: Properties
    let paramsToExclude:[String] = ["super"]
    
    // MARK: Public Functions
    //use reflection to parse PFObjects paramater to common swift style variables
    func updateValues() {
		
		for child in Mirror(reflecting: self).children {
			if let key = child.0 {
				if !paramsToExclude.contains(key) {
					if let value = objectForKey(key) as? String {
						setValue(value, forKey: key)
					}
					else if let value = objectForKey(key) as? Int {
						setValue(value, forKey: key)
					}
					else if let value = objectForKey(key) as? NSNumber {
						setValue(value, forKey: key)
					}
				}
            }
		}
    }
}