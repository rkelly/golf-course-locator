//
//  Utilities.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import UIKit

class Utilities {
    
    // MARK: Properties
    static let plist = "plist"
    
    // MARK: Class Functions
    // Gets pList with the MainBundle and passes data back as a Dictionary
	class func bundlePlistRootAsDictionary(pListName:String, bundleIdentifier:String) -> Dictionary<String, AnyObject>? {
        
        if let bundle = NSBundle(identifier: bundleIdentifier) {
            return bundlePlistRootAsDictionary(pListName, bundle: bundle)
        }
        
        return nil
    }
	
    
    // Gets pList with the MainBundle and passes data back as an Array
    class func bundlePlistRootAsDictionary(pListName:String, bundle:NSBundle = NSBundle.mainBundle()) -> Dictionary<String, AnyObject>? {
        
        if let path = bundle.pathForResource(pListName, ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> {
                return dict
            }
        }
        return nil
    }
    
    // Converts Meters to Miles, 1 Meter = 0.00062137119 Miles
    class func convertMetersToMiles(meters:Double) ->Double{
        
        return meters * 0.00062137119
    }
}