//
//  ParseManager.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/6/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import Parse

let parseManager = ParseManager.sharedInstance
class ParseManager {
    
    // MARK: Properties
    static let localizationTable = "Parse"
    static let errorDomain = "Parse"
    
    let parseApplicationID = "parseApplicationID".parseLocalization()
    let parseClientKey = "parseClientKey".parseLocalization()
    
    // Create Singleton Instance
    class var sharedInstance : ParseManager {
        struct Static {
            static let instance : ParseManager = ParseManager()
        }
        return Static.instance
    }
    
    // MARK: Public Functions
    // Initial Task to ensure proper configuration
    func initialize() {
        
        // Register all sub classes before setting the application
        Course.registerSubclass()
        
        // Fill in with your Parse credentials:
        Parse.setApplicationId(parseApplicationID, clientKey: parseClientKey)
    }
    
    // query that takes in CLLocation object and returns the Courses in that location
    func coursesByLocation(location:CLLocation, withInMiles:Double, completion:(response: [Course]?, error: NSError?)->()){
        
        let query = Course.query()!
        let geoPoint = PFGeoPoint(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        query.whereKey(Course.DataMapping.location, nearGeoPoint: geoPoint, withinMiles: withInMiles)
        query.limit = 20
        
        parseManager.couresQuery(query) { (response, error) -> () in
            
            if let unsortedCourses = response {
                
                let sortedCourses = Course.sortedCoursesByDistance(unsortedCourses, ascending: true, fromLocation: LocationOrigin.Keys.location)
                completion(response: sortedCourses, error: nil)
            }
            else {
                completion(response: response, error: error)
            }
        }
    }
    
    // func thats takes in queries for fetching array of Course Objects
    func couresQuery(query:PFQuery, completion:(response: [Course]?, error: NSError?)->()){
        
        query.addAscendingOrder(Course.DataMapping.name)
        query.findObjectsInBackgroundWithBlock { (response: [AnyObject]?, error: NSError?) -> Void in
            
            if error != nil {
                completion(response: nil, error: error)
            }
            else{
                
                if let objects = response {
                    var courses:[Course] = []
                    for object in objects {
                        
                        if let course = object as? Course {
                            course.updateValues()
                            courses.append(course)
                        }
                    }
                    
                    completion(response: courses, error: nil)
                }
                else {
                    completion(response: nil, error: nil)
                }
            }
        }
    }
}

extension String {
    
    // MARK Public Functions
    // grabs strings form string tables, default file is Parse.strings
    func parseLocalization() -> String {
        return localization(ParseManager.localizationTable)
    }
}

extension NSError {
    
    // MARK: Public Functions
    // Maps the correct error Messages for the Parse API
    func parseLocalizedUserInfo() -> [String: String] {
        
        var localizedDescription: String = ""
        var localizedFailureReasonError: String = ""
        let localizedRecoverySuggestionError: String = ""
        
        
        // If not a Parse Error print out and log
        if self.domain != ParseManager.errorDomain {
            
            print("Not a Parse Error error. Please use UserInfo not parseLocalizedUserInfo.")
            
            return [
                NSLocalizedDescriptionKey: localizedDescription,
                NSLocalizedFailureReasonErrorKey: localizedFailureReasonError,
                NSLocalizedRecoverySuggestionErrorKey: localizedRecoverySuggestionError]
        }
        
        // If a Parse Error check for case and return
        switch self.code {
        case PFErrorCode.ErrorInternalServer.rawValue:
            localizedDescription = "errorInternalServerDescription".parseLocalization()
            localizedFailureReasonError = "errorInternalServerReason".parseLocalization()
            
        case PFErrorCode.ErrorConnectionFailed.rawValue:
            localizedDescription = "errorConnectionFailedDescription".parseLocalization()
            localizedFailureReasonError = "errorConnectionFailedReason".parseLocalization()
            
        case PFErrorCode.ErrorObjectNotFound.rawValue:
            localizedDescription = "errorObjectNotFoundDescription".parseLocalization()
            localizedFailureReasonError = "errorObjectNotFoundReason".parseLocalization()
            
        case PFErrorCode.ErrorInvalidQuery.rawValue:
            localizedDescription = "errorInvalidQueryDescription".parseLocalization()
            localizedFailureReasonError = "errorInvalidQueryReason".parseLocalization()
            
        case PFErrorCode.ErrorInvalidClassName.rawValue:
            localizedDescription = "errorInvalidClassNameDescription".parseLocalization()
            localizedFailureReasonError = "errorInvalidClassNameReason".parseLocalization()
            
        case PFErrorCode.ErrorRequestLimitExceeded.rawValue:
            localizedDescription = "errorRequestLimitExceededDescription".parseLocalization()
            localizedFailureReasonError = "errorRequestLimitExceededReason".parseLocalization()
            
        case PFErrorCode.ErrorInvalidEventName.rawValue:
            localizedDescription = "errorInvalidEventNameDescription".parseLocalization()
            localizedFailureReasonError = "errorInvalidEventNameReason".parseLocalization()
            
        case PFErrorCode.ErrorTimeout.rawValue:
            localizedDescription = "errorTimeoutDescription".parseLocalization()
            localizedFailureReasonError = "errorTimeoutReason".parseLocalization()
            
        case PFErrorCode.ErrorDuplicateValue.rawValue:
            localizedDescription = "errorDuplicateValueDescription".parseLocalization()
            localizedFailureReasonError = "errorDuplicateValueReason".parseLocalization()
            
        default:
            localizedDescription = "errorInternalServerDescription".parseLocalization()
            localizedFailureReasonError = "errorInternalServerReason".parseLocalization()
        }
        
        return [
            NSLocalizedDescriptionKey: localizedDescription,
            NSLocalizedFailureReasonErrorKey: localizedFailureReasonError,
            NSLocalizedRecoverySuggestionErrorKey: localizedRecoverySuggestionError]
    }
}



