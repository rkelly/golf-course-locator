//
//  UIAlertViewController+.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    // MARK: Properties
    enum Heading:String {
        
        case Info = "Information"
        case Warn = "Warning"
        case Error = "Error"
    }
    
    // MARK: Class Functions
    class func QuickMessage(heading:Heading = .Info, message:String) -> UIAlertController {
        
        let alertController = UIAlertController(title: heading.rawValue, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        return alertController
    }
}