//
//  Double+.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation

extension Double {
    
    // MARK: Public Functions
    // displays the given nu,ber of decimals
    func format(f: String) -> String {
        return NSString(format: "%\(f)f", self) as String
    }
}