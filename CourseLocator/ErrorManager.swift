//
//  ErrorManager.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import Parse

let errorManager = ErrorManager.sharedInstance
class ErrorManager {
    
    // MARK: Properties
    // Create Singleton Instance
    class var sharedInstance : ErrorManager {
        struct Static {
            static let instance : ErrorManager = ErrorManager()
        }
        return Static.instance
    }
    
    // MARK: Public Functions
    // Handle NSErrors
    // Would be much more complicated in a full app
    func handleNSErrors(error:NSError, completion:(showUser:Bool, response:String) ->()) {
        
        // Handle Parse errors
        if error.domain == ParseManager.errorDomain {
            
            let userInfo = error.parseLocalizedUserInfo()
            
            if let description = userInfo[NSLocalizedDescriptionKey], reason = userInfo[NSLocalizedFailureReasonErrorKey], suggestion = userInfo[NSLocalizedRecoverySuggestionErrorKey] {
                
                completion(showUser: true, response: "\(description) \(reason) \(suggestion)")
            }
            else {
                
                print("Unaccounted for error: \(error)")
                completion(showUser: false, response: "")
            }
        }
        
        // Handle any errors here
        completion(showUser: true, response: "errorMessage".localization())
    }
    
    // Report any Core Foundation Errors
    // They should print out and be captured by any analytics API
    func reportCFErrors(error:CFError) {
        
        
        let code = CFErrorGetCode(error)
        let domain = CFErrorGetDomain(error)
        let userInfo = CFErrorCopyUserInfo(error)
        
        print("code: \(code), domain: \(domain), userInfo: \(userInfo)")
    }
    
}

