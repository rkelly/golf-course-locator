//
//  CourseCell.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/7/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import UIKit

class CourseCell:UICollectionViewCell {
    
    // MARK: Properties
    static let identifier = "CourseCell"
    static let cellHeight:CGFloat = 80
    
    @IBOutlet weak var courseNameLabel: Label! {
        didSet{
            courseNameLabel.formatKey = TextFormat.Formats.courseTitleFormat
        }
    }
    
    @IBOutlet weak var courseAddressLabel: Label!{
        didSet{
            courseAddressLabel.formatKey = TextFormat.Formats.courseDetailFormat
        }
    }
    
    @IBOutlet weak var courseDistanceLabel: Label! {
        didSet{
            courseDistanceLabel.formatKey = TextFormat.Formats.courseDetailFormat
        }
    }

    var courseForCell:Course?
    private var gradientAdded = false
    
    // MARK: Overrides Functions
    override func didMoveToWindow() {
        super.didMoveToWindow()
        addGradientLine()
    }
    
    // MARK: Public Functions
    // sets cell views from Course values
    func setupCellFromCourse(course:Course) {
        
        clearValues()
        
        self.courseForCell = course
        courseNameLabel.text = course.name
        
        // adding City and State here manually in a real app this would be a calcuated value
        courseAddressLabel.text = "\(course.street) Lincoln, NE \(course.zip)"
        
        // if distance can be calculate format and show
        if let distance = course.distanceFromCourseLocation(LocationOrigin.Keys.location)?.format(".2") {
            courseDistanceLabel.text = "Distance: \(distance) Miles"
        }
        
        if course.selected {
            backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(CGFloat(0.2))
        }
    }
    
    // Reset the reusable cell values
    func clearValues() {
        courseNameLabel.text = ""
        courseAddressLabel.text = ""
        courseDistanceLabel.text = ""
        backgroundColor = UIColor.clearColor()
    }
    
    // Creates gradient line under view
    func addGradientLine() {
        
        if !gradientAdded {
            let line = CAGradientLayer()
            line.frame = CGRect(x: 0, y: bounds.height - 1, width: bounds.width, height: 1)
            
            
            line.locations = [0.0, 0.3, 0.7, 1.0]
            line.colors = [UIColor.lightGrayColor().colorWithAlphaComponent(0).CGColor,
                UIColor.lightGrayColor().CGColor,
                UIColor.lightGrayColor().CGColor,
                UIColor.lightGrayColor().colorWithAlphaComponent(0).CGColor]
            
            layer.addSublayer(line)
            line.startPoint = CGPoint(x: 0.0, y: 0.0)
            line.endPoint = CGPoint(x: 1.0, y: 0.0)
            gradientAdded = true
        }
    }
}