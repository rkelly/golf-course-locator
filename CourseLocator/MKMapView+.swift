//
//  MKMapView+.swift
//  CourseLocator
//
//  Created by Kelly, Raymond on 8/6/15.
//  Copyright (c) 2015 Kelly, Raymond. All rights reserved.
//

import Foundation
import MapKit

extension MKMapView {
    
    // MARK: Public Functions
    
    // Creates a region from location and raduis then sets the mapView
    func updateMapView(location:CLLocation, radius:CLLocationDegrees, animated:Bool) {
        
        let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: radius, longitudeDelta: radius)
        let region:MKCoordinateRegion = MKCoordinateRegion(center: location.coordinate, span: span)
        setRegion(region, animated: animated)
    }
}